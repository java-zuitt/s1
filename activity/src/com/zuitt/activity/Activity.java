package com.zuitt.activity;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){

        Scanner student = new Scanner(System.in);
        System.out.println("What is your first name?");
        String firstName = student.nextLine();
        System.out.println("What is your last name?");
        String lastName = student.nextLine();
        System.out.println("What is your first grade?");
        double firstSubject = student.nextInt();
        System.out.println("What is your second grade?");
        double secondSubject = student.nextInt();
        System.out.println("What is your third grade?");
        double thirdSubject = student.nextInt();
        double average = (firstSubject+secondSubject+thirdSubject)/3;
        System.out.println(firstName + " " + lastName + "'s average grade is: " + average);

    }
}
