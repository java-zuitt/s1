package com.zuitt.example;
import java.util.Scanner;
//this is the functionality that we will use

public class UserInput {

    public static void main(String[] args){

        //we instantiate the myObj from the Scanner class
        //Scanner is used for obtaining input from the terminal
        //System.in allows us to take input from the console

        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter username:");
        //to capture the input given by the user, we will use the nextLine() method;

        String userName = myObj.nextLine();
        System.out.println("Username is: " + userName);

        //System.out.println("Enter a number to add:");
        System.out.println("Enter first number:");
        //int num1 = Integer.parseInt(myObj.nextLine());
        int num1 = myObj.nextInt();
        System.out.println("Enter second number:");
        int num2 = myObj.nextInt();
        System.out.println(num1+num2);

    }
}
