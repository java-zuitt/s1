//package in Java is used to group related classes
//think if it as a folder in a file directory
    //packages are divided into 2 categories
    //1. Built-in Packages (packages from Java API)
    //2. User-defined Packages (create your own package)

//reverse domain name notation
package com.zuitt.example;

public class Variables {
    //for the meantime we will run within the class file
    public static void main(String[] args){

        //Naming conventions
        //the terminology used for variable name is "identifier"
        //Syntax: dataType identifier;

        //variable
        int age;
        char middleName;

        int x, y = 1;

        //initialization after declaration
        x = 1;

        //change values
        y = 2;

        //
        System.out.println("The value of y is " + y + " and the value of x is " + x + ".");

        //Primitive Data Types
        //predefined within the Java programming language which is used for single-valued variables with limited capabilities

        //int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long
        //L is added to the end of the long numbers to be recognized
        long worldPopulation = 987654312351245L;

        //float
        //add f at the end of a float
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        //double - floating point values
        double piDouble = 3.14159265359;
        System.out.println(piDouble);

        //char - single characters
        char letter = 'c';
        System.out.println(letter);

        //boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //constant
        //final - keyword
        //common practice - CAPITAL LETTERS for readability
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        //PRINCIPAL = 4000;

        //non-primitive data types
        //can store multiple values
        //aka as reference data types - refer to instances or objects
        //do not directly store the value of a variable, but rather remembers the reference to the variable

        //String
        //stores a sequence of array of characters
        String userName = "CardoD";
        System.out.println(userName);
        //Sample String Methods
        int stringLength = userName.length();
        System.out.println(stringLength);



    }
}
